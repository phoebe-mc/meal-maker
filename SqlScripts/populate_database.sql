USE meal_maker;


INSERT INTO users (firstName, surname, email, password) VALUES ('Phoebe','McBride','phoebemcb@hotmail.com','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');
INSERT INTO users (firstName, surname, email, password) VALUES ('Helen', 'Boyle','helenb@yahoo.com','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');
INSERT INTO users (firstName, surname, email, password) VALUES ('Connor','Smyth','csmyth@hotmail.co.uk','e38ad214943daad1d64c102faec29de4afe9da3d');
INSERT INTO users (firstName, surname, email, password) VALUES ('Thomas','Faulkner','tucker@hotmail.com','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');
INSERT INTO users (firstName, surname, email, password) VALUES ('Anne','McAuley','anne@gmail.com','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');
INSERT INTO users (firstName, surname, email, password) VALUES ('Adam','Dickey','adamd@outlook.com','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');
INSERT INTO users (firstName, surname, email, password) VALUES ('Clodagh','ONeill','clo@hotmail.com','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');
INSERT INTO users (firstName, surname, email, password) VALUES ('Kevin','Coll','kcoll@gmail.com','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');
INSERT INTO users (firstName, surname, email, password) VALUES ('Andrew','Sweeney','sweendog@hotmail.com','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');
INSERT INTO users (firstName, surname, email, password) VALUES ('Conal','McNulty','conalMc@outlook.com','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');
INSERT INTO users (firstName, surname, email, password) VALUES ('Jamie','Dennison','jdennison06@outlook.com','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');
INSERT INTO users (firstName, surname, email, password) VALUES ('Siobhan','McBride','shivmcb@hotmail.com','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');
INSERT INTO recipes (title, ingredients, method, isPublic, imagePath, userId, dateOfEntry) VALUES 
('Vegan Shepherd''s Pie','600 g Maris Piper potatoes
600 g sweet potatoes 
40 g dairy-free margarine
1 onion
2 carrots
3 cloves of garlic
2 sticks of celery
1 tablespoon coriander seeds
olive oil
½ a bunch of fresh thyme
350 g chestnut mushrooms
12 sun-dried tomatoes
2 tablespoons balsamic vinegar
vegan red wine
100 ml organic vegetable stock
1 x 400 g tin of lentils
1 x 400 g tin of chickpeas
5 sprigs of fresh flat-leaf parsley
2 sprigs of fresh rosemary
1 lemon
30 g fresh breadcrumbs','Preheat the oven to 200°C/400°F/gas 6.
Peel and chop all the potatoes into rough 2cm chunks. Place the Maris Pipers into a large pan of cold salted water over a medium heat. Bring to the boil, then simmer for 10 to 15 minutes, or until tender, adding the sweet potatoes after 5 minutes.
Drain and leave to steam dry, then return to the pan with the margarine and a pinch of sea salt and black pepper. Mash until smooth, then set aside.
Peel and finely slice the onion, carrots and 2 garlic cloves, then trim and finely slice the celery.
Bash the coriander seeds in a pestle and mortar until fine, then add it all to a medium pan over a medium heat with a good splash of oil. Pick in the thyme leaves, then cook for around 10 minutes, or until softened.
Meanwhile, roughly chop the mushrooms and sun-dried tomatoes, then add to the pan along with the vinegar and 2 tablespoons of the sun-dried tomato oil from the jar.
Cook for a further 10 minutes, then add a splash of wine, turn up the heat, and allow it to bubble away. Stir in the stock, lentils and chickpeas (juice and all), then leave it to tick away for 5 to 10 minutes, or until slightly thickened and reduced.
Pick and roughly chop the parsley leaves, then stir into the pan. Season to taste, then transfer to a baking dish (roughly 25cm x 30cm).
Spread the mash over the top, scuffing it up with the back of a spoon.
Finely slice the remaining garlic clove, then place into a bowl with the rosemary leaves, lemon zest, breadcrumbs and 1 tablespoon of oil. Mix well, sprinkle over the mash, then place in the hot oven for around 10 minutes, or until piping hot through.
Place under the grill for a further 2 to 3 minutes, or until golden, then serve with seasonal greens.'
,1,'shepherdsPie.jpg', 1, NOW());

INSERT INTO recipes (title, ingredients, method, isPublic, imagePath, userId, dateOfEntry) VALUES 
('Vegan Chilli','2 tablespoons olive oil
1 medium red onion, chopped
1 large red bell pepper, chopped
2 medium carrots, chopped
2 ribs celery, chopped
½ teaspoon salt, divided
4 cloves garlic, pressed or minced
2 tablespoons chili powder
2 teaspoons ground cumin
1 ½ teaspoons smoked paprika
1 teaspoon dried oregano
1 large can (28 ounces) or 2 small cans (15 ounces each) diced tomatoes*, with their juices
2 cans (15 ounces each) black beans, rinsed and drained
1 can (15 ounces) pinto beans, rinsed and drained
2 cups vegetable broth or water
1 bay leaf
2 tablespoons chopped fresh cilantro, plus more for garnishing
1 to 2 teaspoons sherry vinegar or red wine vinegar or lime juice, to taste
Garnishes: chopped cilantro, sliced avocado, tortilla chips, sour cream or crème fraîche, grated cheddar cheese, etc.','In a large Dutch oven or heavy-bottomed pot over medium heat, warm the olive oil until shimmering. Add the chopped onion, bell pepper, carrot, celery and ¼ teaspoon salt. Stir to combine and then cook, stirring occasionally, until the vegetables are tender and the onion is translucent, about 7 to 10 minutes.
Add the garlic, chili powder, cumin, smoked paprika (go easy on the paprika if you’re sensitive to spice) and oregano. Cook until fragrant while stirring constantly, about 1 minute.
Add the diced tomatoes and their juices, the drained black beans and pinto beans, vegetable broth and bay leaf. Stir to combine and let the mixture come to a simmer. Continue cooking, stirring occasionally and reducing heat as necessary to maintain a gentle simmer, for 30 minutes. Remove the chili from heat.
For the best texture and flavor, transfer 1 ½ cups of the chili to a blender and blend until smooth, then pour the blended mixture back into the pot. (Or, you can blend the chili briefly with an immersion blender, or mash the chili with a potato masher until it reaches a thicker, more chili-like consistency.)
Add the chopped cilantro, stir to blend, and then mix in the vinegar, to taste. Add salt to taste, too—I added ¼ teaspoon more at this point. Divide the mixture into individual bowls and serve with garnishes of your choice. This chili will keep well in the refrigerator for about 4 days (I haven’t tried, but I bet it would freeze well, too).'
,2,'veganChilli.png', 3, NOW());

INSERT INTO recipes (title, ingredients, method, isPublic, imagePath, userId, dateOfEntry) VALUES 
('Veggie noodles with curried coconut sauce', '2 green courgettes
2 yellow courgettes
1 large carrot
2 corn cobs (kernels only)
200 g fresh peas or mangetout
1 large handful of mixed herbs, such as coriander, flat-leaf parsley, rosemary, oregano, thyme
CURRIED COCONUT SAUCE
1 small banana shallot
1 small clove of garlic
3 cm piece of ginger
½ a fresh green chilli
5 cm piece of turmeric , or 2 tesapoons ground turmeric
1 lime , plus extra to serve
200 ml coconut milk
300 ml coconut water
100 g unsweetened desiccated coconut
1 teaspoon medium-hot curry powder','First make the sauce. Peel and roughly chop the shallot, garlic and ginger, and roughly chop the chilli. Juice the turmeric, if using fresh. Zest and juice the lime.
Blitz all the sauce ingredients, except one tablespoon of desiccated coconut, in a food processor until combined. Season to taste – the sauce should be smooth and creamy.
Using a julienne peeler or spiraliser, cut the courgettes and carrot into long noodles. Place in a bowl with the rest of the vegetables, slicing the mangetout diagonally (if using).
Pour over the sauce and mix well. Pick, finely chop and sprinkle over the herbs and reserved tablespoon of desiccated coconut .
Leave to marinate for 30 minutes, until the ‘noodles’ have softened slightly, then serve with lime wedges for squeezing over.'
,1,'veggieNoodles.png', 2, NOW());

INSERT INTO recipes (title, ingredients, method, isPublic, imagePath, userId, dateOfEntry) VALUES 
('Cauliflower dahl', '4 shallots
1 clove of garlic
1 small cauliflower
groundnut oil
1 small handful of curry leaves
2 teaspoons mustard seeds
½ dried red chilli
300 g yellow split peas
2 x 400 g tins of reduced fat coconut milk','Peel and slice the shallots and garlic, then cut the cauliflower into florets.
Heat some oil in a pan and fry most of the shallots and the garlic until soft, then add most of the curry leaves and half the mustard seeds, crumble in the chilli and fry for 2 minutes.
Add the cauliflower, split peas and coconut milk, then fill 1 empty tin with water and add to the pan. Season, bring to the boil, then lower the heat and simmer for 40 minutes, stirring occasionally. If the liquid reduces too much, add a splash of water.
Fry the remaining shallots in oil on a high heat until crisp, then add the remaining curry leaves and mustard seeds and fry for 1 minute. Serve dhal scattered with the crisp shallot mixture.'
,1,'curriedDahl.png', 1, NOW());

INSERT INTO recipes (title, ingredients, method, isPublic, imagePath, userId, dateOfEntry) VALUES 
('Bacon bolognese', '400g spaghetti
1 tsp olive oil
2 large carrots
, finely diced
3 celery
 sticks, finely diced
200g pack smoked bacon lardon
190g jar sundried tomato pesto
8-12 basil leaves, shredded (optional)',
'Boil the spaghetti following pack instructions. Meanwhile, heat the oil in a non-stick pan. Add the carrots, celery and bacon, and stir well. Cover the pan and cook, stirring occasionally, for 10 mins until the veg has softened.
Tip in the pesto, warm through, then stir through the drained spaghetti with the basil, if using.'
,1,'baconBolognese.png', 2, NOW());

INSERT INTO recipes (title, ingredients, method, isPublic, imagePath, userId, dateOfEntry) VALUES 
('Pea and bacon pasties', '200g pack smoked bacon lardon, cut into small pieces
225g frozen pea
140g mascarpone
25g parmesan
, grated, plus a little extra for the tops
2 medium eggs
small handful mint
, leaves chopped
375g pack puff pastry
plain flour, for rolling',
'Put the lardons in a large frying pan, cook until crisp, about 8 mins, then drain on kitchen paper. Meanwhile, pour kettle-hot water over the peas and leave to stand for 5 mins, then drain well.
Heat oven to 200C/180C fan/gas 6. When the bacon and peas have cooled, combine in a large bowl. Mash lightly to crush the peas and break up the bacon. Stir in the mascarpone, Parmesan, 1 egg and the mint, then season with black pepper.
Roll out the pastry on a lightly floured surface to the thickness of a 20p coin. Cut circles out using an 12cm cutter. Spoon the filling into the centre of each. Brush the edges with beaten egg and pinch the pastry together on one side to seal. Brush the tops with a little more egg wash and sprinkle each with Parmesan. Place the pasties on 2 floured baking sheets. Bake for 25 mins. Leave to cool, then chill until ready to serve or pack up.'
,1,'peaBaconPasties.png', 3, NOW());

INSERT INTO recipes (title, ingredients, method, isPublic, imagePath, userId, dateOfEntry) VALUES 
('Beer mac n cheese', '50g butter
2 garlic cloves, grated
50g spinach
500ml milk
250ml pale ale
220g macaroni
100g cheddar
, grated
200g mozzarella, grated',
'Heat the grill to its highest setting. In a flameproof casserole dish or metal frying pan, melt the butter and fry the garlic for 2 mins. Add the spinach and cook until it wilts, about 2 mins. Pour in the milk and beer and bring to a gentle bubble. Add the macaroni and stir intermittently for around 20 mins until the pasta is cooked and covered in sauce.
Stir in the cheddar and half the mozzarella. When it starts to melt, sprinkle the remaining mozzarella on top and put it in the oven for around 7-10 mins or until browned on top and sensational underneath.'
,1,'beerMacCheese.png', 12, NOW());

INSERT INTO recipes (title, ingredients, method, isPublic, imagePath, userId, dateOfEntry) VALUES 
('Cheese & bacon scones', '100g butter
, plus extra for greasing
10 rashers streaky bacon
275g self-raising flour
½ tsp baking powder
150ml milk
50ml vegetable oil
1 egg
handful snipped chives
150g grated cheddar',
'Heat oven to 200C/180C fan/gas 6 and grease a 12-hole muffin tin. In a frying pan over a medium heat, fry the bacon for 5 mins until golden. Let it cool, then chop into chunks and set aside
In a bowl, combine the flour, baking powder and 1 tsp sea salt. Using your fingers, mix the butter into the flour mixture until it resembles breadcrumbs
In a small bowl, whisk together the milk, oil and egg. Tip into the dry mixture, and gently mix until the flour mixture is mostly moistened (lumps will remain). Stir in the bacon, chives and cheese, then spoon the batter into the muffin tin.
Put the muffin tin in the oven and bake for 20 mins or until the tops are golden brown. Serve warm.'
,1,'cheeseBaconScones.png', 14, NOW());

INSERT INTO recipes (title, ingredients, method, isPublic, imagePath, userId, dateOfEntry) VALUES 
('Tomato & chickpea curry', '1 tbsp olive oil
2 onions
, finely sliced
2 garlic cloves, crushed
1 tsp garam masala
1 tsp turmeric
1 tsp ground coriander
400g can plum tomatoes
400ml can coconut milk
400g can chickpeas, drained and rinsed
2 large tomatoes
, quartered
½ small pack coriander, roughly chopped
cooked basmati rice, to serve',
'Heat the oil in a large pan and add the onions. Cook until softened, about 10 mins. Add the garlic and spices, and stir to combine. Cook for 1-2 mins, then pour in the canned tomatoes, break up with a wooden spoon and simmer for 10 mins.
Pour in the coconut milk and season. Bring to the boil and simmer for a further 10-15 mins until the sauce has thickened.
Tip in the chickpeas and the tomatoes, and warm through. Scatter over the coriander and serve with fluffy rice.'
,1,'tomatoChickpeaCurry.png', 8, NOW());

INSERT INTO recipes (title, ingredients, method, isPublic, imagePath, userId, dateOfEntry) VALUES 
('Harissa roasted tomatoes with couscous', '12 big plum tomatoes, halved
1 tbsp harissa
3 tbsp olive oil
3 onions
, very thinly sliced
4 tbsp Greek-style yogurt
1 tbsp tahini paste
1 garlic clove, crushed
200g couscous
½ small pack mint
, roughly chopped
½ small pack parsley
, roughly chopped
50g toasted flaked almond
400g can chickpea, drained and rinsed',
'Heat oven to 200C/180C fan/gas 6. Toss the tomatoes in harissa and 2 tbsp of the oil. Season and spread in a roasting tin, cut-side up, and bake for 40-45 mins.
Heat the remaining oil in a large frying pan. Tip in the onions and sizzle for a couple of mins. Turn down the heat, season and cook for 15 mins or until golden and caramelised. In a bowl, mix the yogurt, tahini and garlic with some seasoning. Set aside.
Tip the couscous into a large bowl. Pour over 400ml boiling water. Cover with cling film and leave to stand for 10 mins or until all the water has been absorbed. Fork though the herbs, almonds, chickpeas and half the onions. Top with the tomatoes and the remaining onions, and serve with a dollop of the yogurt sauce.'
,1,'harissaTomatoes.png', 9, NOW());

INSERT INTO recipes (title, ingredients, method, isPublic, imagePath, userId, dateOfEntry) VALUES 
('Boiled eggs & asparagus soldiers', '1 large bunch of asparagus , (16 spears)
8 slices of higher-welfare pancetta
4 large free-range eggs
4 slices of wholemeal bread , (50g each)',
'Place a griddle pan over a high heat to heat up.
Snap the woody ends off the asparagus and discard.
Fill a medium saucepan three-quarters of the way up with water, add a pinch of sea salt, then place over a high heat and bring to a fast boil.
On a chopping board, slice the pancetta in half across the middle, so you end up with 16 pieces, then gently stretch out each one.
Wrap each asparagus spear in a piece of pancetta, then place onto the hot griddle and cook for 5 minutes, or until the asparagus is tender and the pancetta is crisp, turning occasionally.
One at a time, place the eggs onto a spoon, then carefully dip in and out of the boiling water a few times – dipping will reduce the temperature shock and stop them from cracking – then gently lower into the pan.
Cook the eggs for 5½ minutes so that they’re runny for dunking – set a timer.
Carefully remove the eggs from the water and place into egg cups set on a plate, then divide up the asparagus.
Meanwhile, quickly toast the bread on the griddle for 30 seconds on each side. Remove from the pan, slice into soldiers on a clean board and divide between your plates.
Lightly tap and remove the top of each egg, season with black pepper, then tuck in.'
,1,'eggAsparagus.png', 10, NOW());