create database meal_maker;

use meal_maker;

create table users
(
  userid int unsigned not null auto_increment primary key,
  firstName varchar(50) not null,
  surname varchar(50) not null,
  email varchar(100) not null,
  password varchar(70) not null
);

create table recipes
(
  recipeid int unsigned not null auto_increment primary key,
  title text not null,
  ingredients text not null,
  method text not null,
  isPublic int not null,
  imagePath char(100) not null,
  userId int not null,
  mealType int not null,
  dateOfEntry datetime not null default CURRENT_TIMESTAMP,
  FOREIGN KEY (userId) REFERENCES users(userId)
);


grant select, insert, update, delete
on meal_maker.*
to meal_maker@localhost identified by 'password';
