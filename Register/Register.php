<?php

require_once('../Html/redirect.php');
if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

$firstName = $_POST['firstName'];
$surname = $_POST['surname'];
$email = $_POST['email'];
$password = $_POST['password'];
$confirmPassword = $_POST['confirmPassword'];
$missingFields = array();
$validationErrors = array();
$insertedId;

// $id = '';
DEFINE('id', '');


				
					// hash password 
					$passwordHash = sha1($password);
				
					// database connection
					@$db = new mysqli('localhost', 'root', 'password123', 'meal_maker');
						
						
					if (mysqli_connect_errno())
						{
							echo "<p>Error: Could not connect to database.<br/>
							Please try again later.</p>";
							exit;
						
						
						}
						
					// all information entered validation + correct format validation per field
					if (empty($firstName))
					{
						array_push($missingFields, "First name");
						
					}else if(!preg_match("/^([A-Za-z]+)$/", $firstName))
					{
						array_push($validationErrors, "First name must contain only alphabetic characters.");
					}
					
					if(empty($surname))
					{
						array_push($missingFields, "Surname");
						
					}else if(!preg_match("/^([A-Za-z]+)$/", $surname))
					{
						array_push($validationErrors, "Surname must contain only alphabetic characters.");
					}
					
					if(empty($email))
					{
						array_push($missingFields, "Email");
						
					}else if (!filter_var($email, FILTER_VALIDATE_EMAIL))
					{
						array_push($validationErrors, "Email address is invalid.");
					}
					
					if(empty($password) || empty($confirmPassword))
					{
						array_push($missingFields, "Password and confirm password");
						
					} else if($password != $confirmPassword)
					{
						array_push($validationErrors, "Passwords do not match.");
					}
					
					// output collected missing errors
					if($missingFields != null)
					{
						echo "The following fields are required to continue: </br>";
						foreach($missingFields as $missing)
						{
							echo "$missing </br>";
						}
						echo "</br>";
					}
					
					// output collected validation errors
					if($validationErrors != null)
					{
						echo "Please correct the following errors:</br>";
						foreach($validationErrors as $error)
						{
							echo "$error </br>";
						}
					}
					
					echo "</br>";
					
					// insert validated values into database		
					
					if($validationErrors == null && $missingFields == null)
					{
						$query = "INSERT IGNORE INTO users VALUES(?, ?, ?, ?, ?)";
						$stmt = $db->prepare($query);
						if (!$stmt) 
							{
								echo $db->error;
							}else
							{
								
								$stmt->bind_param('issss', $id, $firstName, $surname, $email, $passwordHash);
								$stmt->execute();
								$count = $stmt->affected_rows;
								if($count == 0) 
								{
									echo "This email address has already been registered. Please enter a different email address to continue.";
									exit;
								}
								$insertedId = $db->insert_id; 
								$_SESSION['id'] = $insertedId;
								
							}
						
							
							$_SESSION['valid_user'] = $firstName;
					
					redirect_if_required();
					}
					
					
					?>
					
