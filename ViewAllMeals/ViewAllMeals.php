<?php

// include function files for this application
require_once('../Html/navigation.php');

session_start();

define("DS", DIRECTORY_SEPARATOR);
define('ABSPATH', dirname(__FILE__) . DS);
define('LOGINPATH','../Login/Login.html');

if(!isset($_SESSION['valid_user'])){ //if login in session is not set
    header("Location: login.html");
}

$db_conn = new mysqli('localhost', 'root', 'password123', 'meal_maker');

if (mysqli_connect_errno())
		{
			echo 'Connection to database failed:'. mysqli_connect_error();
			exit();
		}
		
		$query = "select * from recipes where isPublic = 1";
		
		$recipeResult = $db_conn->query($query);
		
		$recipe_num_rows = $recipeResult->num_rows;
		
		$db_conn->close();
		

// show navigation bar
show_nav();
?>
		<div class="container">
		<table class="table">
						<thead>
							<tr>
								<th></th>
								<th>Title</th>
								<th>Meal type</th>
								<th>Date uploaded</th>
							</tr>
						</thead>
						<tbody>
						
						
						<?php
							if( $recipe_num_rows == 0 )
							{
							echo '<tr><td colspan="4">No recipes yet</td></tr>';
							}else
							{
								while($row = $recipeResult->fetch_assoc())
								{
									switch($row['mealType']){
										
										case 0: $mealType = "Breakfast";
										break;
										case 1: $mealType = "Lunch";
										break;
										case 2: $mealType = "Dinner";
										break;
										case 3: $mealType = "Snack";
										break;
									}
									
									echo "<tr><td><img src=\"../Images/{$row['imagePath']}\"
              style=\"border: 1px solid black\" height=\"200\" width=\"200\"/></td><td><a href=\"..\ViewMeal\ViewMeal.php?recipeid={$row['recipeid']}\">{$row['title']}</td><td>$mealType</td><td>{$row['dateOfEntry']}</td></tr>\n";
								}
							}
						?>
				</tbody>
			</table>
		
		
		
		</div>
	</body>
</html>
