
<?php
session_start();

function redirect_if_required()
{	
	if(!isset($_SESSION['id'])){
		
		header("Location: ../Login/Login.html"); 
		exit;
	}else
	{
		header("Location: ../MyMeals/MyMeals.php");
		exit;
	}
}
?>