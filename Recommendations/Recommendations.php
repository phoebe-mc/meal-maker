<?php

// include function files for this application
require_once('../Html/navigation.php');
if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
	$recipeId;
    $userId = $_SESSION['id'];
	// to hold all relevent strings
	$terms = array();
	$recipeIds = array();
	$mealType;

	$db_conn = new mysqli('localhost', 'root', 'password123', 'meal_maker');

	if (mysqli_connect_errno())
	{
		echo 'Connection to database failed:'. mysqli_connect_error();
		exit();
	}
	
	$query = "select * from recipes where userId = '$userId'";
	
	$recipeResult = $db_conn->query($query);
	
	$recipe_num_rows = $recipeResult->num_rows;

	while($row = $recipeResult->fetch_assoc())
	{
					
		$ingredientsSeperated = explode( ' ', $row['ingredients']);						
			
		foreach($ingredientsSeperated as $index => $word)
		{
			if(isValidWord($word)) 
			{ 
				$terms[] = $word;
			}
		}		
			
		$terms = array_unique($terms);
			
		foreach($terms as $index => $termToCheck)
		{		
		$findRecommendations = "select recipeid from recipes WHERE isPublic = 1 AND userId != $userId AND lower(ingredients) like '" . $termToCheck . "%' ";
			
		$recommendationsResult = $db_conn->query($findRecommendations);
			
		$row = $recommendationsResult->fetch_assoc();
		
		if(isset($row["recipeid"]))
		{
		$recipeIds[] = $row["recipeid"];
		
		}	

         	
		}	
		$recipeIds = array_unique($recipeIds);
		
	}

	function isValidWord($word)
	{
		if(is_numeric($word || (strlen($word) < 4)))
		{
			return false;
		}else
		{
			return true;
		}
	}

	show_nav();
?>

<div class="container">
		<table class="table">
						<thead>
							<tr>
								<th></th>
								<th>Title</th>
								<th>Meal type</th>
								<th>Date uploaded</th>
							</tr>
						</thead>
						<tbody>
						
						<?php
						
						foreach($recipeIds as $index => $recipeId)
						{
							$selectRecipe = "select * from recipes where recipeId = '$recipeId'";
							$recommendedRecipe = $db_conn->query($selectRecipe);
							$row = $recommendedRecipe->fetch_assoc();
							
							switch($row['mealType']){
										
										case 0: $mealType = "Breakfast";
										break;
										case 1: $mealType = "Lunch";
										break;
										case 2: $mealType = "Dinner";
										break;
										case 3: $mealType = "Snack";
										break;
									}
									
									echo "<tr><td><img src=\"../Images/{$row['imagePath']}\"
              style=\"border: 1px solid black\" height=\"200\" width=\"200\"/></td><td><a href=\"..\ViewMeal\ViewMeal.php?recipeid={$row['recipeid']}\">{$row['title']}</td><td>$mealType</td><td>{$row['dateOfEntry']}</td></tr>\n";
						}
						
						?>