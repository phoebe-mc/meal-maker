<?php

// include function files for this application
require_once('../Html/navigation.php');
show_nav();
?>


	  <div class="container">
		<form action="AddMeal.php" method="post" enctype="multipart/form-data">
			<h1>Add meal</h1>
					
			<table class="table">
				<tr>	
					<div class='form-group'>
						<td><label>Title</label></td>
						<td><input type="text" name="title"td>
					</div>
				</tr>
				
				<tr>	
					<div class='form-group'>
						<td><label>Ingredients</label></td>
						<td><textarea rows="6" cols="50" name="ingredients"></textarea></td>
					</div>
				</tr>
				
				<tr>	
					<div class='form-group'>
						<td><label>Method</label></td>
						<td><textarea rows="6" cols="50" name="method"></textarea></td>
					</div>
				</tr>	
					
				<tr>	
					<div class='form-group'>
						<td><label>Meal type</label></td>
						<td><select name="mealType">
							<option value="0">Breakfast</option>
							<option value="1">Lunch</option>
							<option value="2">Dinner</option>
							<option value="3">Snack</option>
						</td>
					</div>
				</tr>	
				<tr>
					<div class="form-group">
						<td><label>Who can see your meal?<label></td>
						<td>
							<input type="radio" name="isPublic" value="0">Just me
							<input type="radio" name="isPublic" value="1">Everyone
						</td>
					</div>
				</tr>
				<tr>
					<td>
						<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
						<label for="the_file">Upload a file:</label>
					</td>
					<td>
						<input type="file" name="the_file" id="the_file"/>
						</form>
					</td>					
				</tr>
				<tr>	
					<div class='form-group'>
						<td></td>
						<td><input type="submit" value="Add meal" /></td>
					</div>
				</tr>	
			</table>	
		</form>
		</div>
	</body>
</html>