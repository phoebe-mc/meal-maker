<?php
// include function files for this application
require_once('../Html/navigation.php');
if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

define("DS", DIRECTORY_SEPARATOR);
define('ABSPATH', dirname(__FILE__) . DS);
define('LOGINPATH','../Login/Login.html');

if(!isset($_SESSION['valid_user'])){ //if login in session is not set
    header("Location: login.html");
}

$title = $_POST['title'];
$method = $_POST['method'];
$ingredients = $_POST['ingredients'];
$mealType = $_POST['mealType'];
$dateAdded;
$missingFields = array();
$validationErrors = array();
$nameOfFile = $_FILES['the_file']['name'];
$userId = $_SESSION['id'];

// $id = '';
DEFINE('id', '');
DEFINE('dateRegistered', '');

?>

<!DOCTYPE html>
<?php
show_nav();
?>
	  <body>
 <div class="container">
    <?php
	
	if ($_FILES['the_file']['error'] > 0)
	{
		echo 'Problem: ';
		switch ($_FILES['the_file']['error'])
			{
				 case 1: echo 'File exceeded upload_max_filesize.';
						 break;
				 case 2: echo 'File exceeded max_file_size.';
						 break;
				 case 3: echo 'File only partially uploaded.';
						 break;
				 case 4: echo 'No file uploaded.';
						 break;
				 case 6: echo 'Cannot upload file: No temp directory specified.';
						 break;
				 case 7: echo 'Upload failed: Cannot write to disk.';
						 break;
			}
		exit;
	}
	// Does the file have the right MIME type?
		 if ($_FILES['the_file']['type'] != 'image/png')
		 {
			echo 'Problem: file is not a PNG image.';
			exit;
		 }
		 
	
	define('UPLOAD_PATH','../Images/');
	
	// put the file where we'd like it
	$uploaded_file = ABSPATH . UPLOAD_PATH .$_FILES['the_file']['name'];

	 if (is_uploaded_file($_FILES['the_file']['tmp_name']))
	 {
		if(!move_uploaded_file($_FILES['the_file']['tmp_name'],$uploaded_file))
		{
			 echo "Problem: Could not move file to destination directory.";
			 exit;
		}
	 }
	 else
	 {
		echo "Problem: Possible file upload attack. Filename: ";
		echo $_FILES['the_file']['name'];
		exit;
	 }
	
		// database connection
		@$db = new mysqli('localhost', 'root', 'password123', 'meal_maker');
			
			
		if (mysqli_connect_errno())
			{
				echo "<p>Error: Could not connect to database.<br/>
				Please try again later.</p>";
				exit;
			
			
			}
			
		// all information entered validation + correct format validation per field
		if (empty($title))
		{
			array_push($missingFields, "Title");
			
		}
		
		if(empty($ingredients))
		{
			array_push($missingFields, "Ingredients");
			
		}
		
		if(empty($method))
		{
			array_push($missingFields, "Method");
			
		}
		
		if($missingFields != null)
		{
			echo "<h1>Errors in submission</h1>";
		}
		
		// output collected missing errors
		if($missingFields != null)
		{
			echo "The following fields are required to continue: </br>";
			foreach($missingFields as $missing)
			{
				echo "$missing </br>";
			}
			echo "</br>";
		}
		
		// output collected validation errors
		if($validationErrors != null)
		{
			echo "Please correct the following errors:</br>";
			foreach($validationErrors as $error)
			{
				echo "$error </br>";
			}
		}
		
		// insert validated values into database		
		
		if($validationErrors == null && $missingFields == null)
		{
			echo "<h1>Meal added successfully!</h1>";
			$query = "INSERT IGNORE INTO recipes VALUES(?, ?, ?, ?, ?, ?, ?, ?, " . "NOW()" . ")";
			$stmt = $db->prepare($query);
			if (!$stmt) 
				{
					echo $db->error;
				}else
				{
					
					$stmt->bind_param('isssisii', $id, $title, $ingredients, $method, $isPublic, $nameOfFile, $userId, $mealType);
					$stmt->execute();
					$count = $stmt->affected_rows;
					if($count == 0) 
					{
						echo "Didn't work";
						exit;
					}
					
				}
		}
		?>
		</div>
	</body>
</html>