<?php
session_start();

// store to test if they *were* logged in
$old_user = $_SESSION['valid_user'];

unset($_SESSION['valid_user']);
session_destroy();

?>
<!DOCTYPE html>
<html>
	<head>
		 <title>Log Out</title>
		 <!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	</head>
	<body>
		<div class="container">
			<h1>Log Out</h1>
			<?php
			if (!empty($old_user))
			{
				echo '<p>You have been logged out.</p>';
			}
			else
			{
				 // if they weren't logged in but came to this page somehow
				 echo '<p>You were not logged in, and so have not been logged out.</p>';
			}
			?>
			<p><a href="Login.html">Back to Login Page</a></p>
		</div>
	</body>

</html>