<?php

// include function files for this application
require_once('../Html/navigation.php');
if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
$userId = $_SESSION['id'];
$userToDisplay = $_SESSION['valid_user'];

$db_conn = new mysqli('localhost', 'root', 'password123', 'meal_maker');

# Prepare the SELECT Query
					$selectSQL = "SELECT * FROM recipes WHERE userId = '$userId'";		
					
					$recipeResult = $db_conn->query($selectSQL);
					$recipe_num_rows = $recipeResult->num_rows;
					

	$db_conn->close();

// show navigation bar
show_nav();
?>


<div class="container">
		<a href="../AddMeal/AddMealForm.php" class="btn btn-default btn-lg" role="button">Add meal</a>
		</br>
		<table class="table">
						<thead>
							<tr>
								<th></th>
								<th>Title</th>
								<th>Meal type</th>
								<th>Date uploaded</th>
							</tr>
						</thead>
						<tbody>
						
						
						<?php

							if( $recipe_num_rows ==0 )
							{
							echo '<tr><td colspan="4">No recipes yet</td></tr>';
							}else
							{
								while($row = $recipeResult->fetch_assoc())
								{
									switch($row['mealType']){
										
										case 0: $mealType = "Breakfast";
										break;
										case 1: $mealType = "Lunch";
										break;
										case 2: $mealType = "Dinner";
										break;
										case 3: $mealType = "Snack";
										break;
									}
									
									echo "<tr><td><img src=\"../Images/{$row['imagePath']}\"
              style=\"border: 1px solid black\" height=\"200\" width=\"200\"/></td><td><a href=\"..\ViewMeal\ViewMeal.php?recipeid={$row['recipeid']}\">{$row['title']}</a></td><td>$mealType</td><td>{$row['dateOfEntry']}</td></tr>\n";
								}
							}
						?>
				</tbody>
			</table>
		
		
		
		</div>
	</body>
</html>