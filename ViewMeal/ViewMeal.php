<?php
// include function files for this application
require_once('../Html/navigation.php');

if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
	

$userId = $_SESSION['id'];
$userToDisplay = $_SESSION['valid_user'];

$recipeid = isset($_GET["recipeid"]) ? $_GET["recipeid"] : false;
if ($recipeid === false) {
    exit("no recipe provided");
}

$db_conn = new mysqli('localhost', 'root', 'password123', 'meal_maker');

# Prepare the SELECT Query
					$selectSQL = "SELECT * FROM recipes WHERE recipeid = '$recipeid'";		
					
					$recipeResult = $db_conn->query($selectSQL);
					$recipe_num_rows = $recipeResult->num_rows;
				
			$db_conn->close();	
// show navigation bar
show_nav();

if( $recipe_num_rows ==0 )
							{
							echo "nothing here";
							}else
							{
								while($row = $recipeResult->fetch_assoc())
								{
									switch($row['mealType']){
										
										case 0: $mealType = "Breakfast";
										break;
										case 1: $mealType = "Lunch";
										break;
										case 2: $mealType = "Dinner";
										break;
										case 3: $mealType = "Snack";
										break;
									}
								

?>
<div class="container">
		<h1><?php echo "{$row['title']}"?></h1>
		<table class="table">
						<tbody>
        <tr>
          <th></th>
            <td><img src="../Images/<?php echo"{$row['imagePath']}"?>" style="border: 1px solid black" height="200" width="200"/></td>           
        </tr>
        <tr>
          <th>Ingredients</th>
            <td><?php echo "{$row['ingredients']}"?></td>
        </tr>
        <tr>
          <th>Method</th>
            <td><?php echo "{$row['method']}"?></td>
        </tr>
      <tr>
          <th>Meal type</th>
            <td><?php echo "$mealType"?></td>
        </tr>
		<tr>
          <th>Date entered</th>
            <td><?php echo "{$row['dateOfEntry']}"?></td>
        </tr>
    </tbody>
<?php

								}
							}
							
							?>